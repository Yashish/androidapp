package com.example.yashish.pressureinsole.bletest;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yashish.pressureinsole.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Yashish on 12/15/2015.
 */

public class PressureSensorControl2 extends Activity {
     private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    public int[] myGlobalArray = new int[16];
    public static int[] myGlobalArray2 = new int[8];

    public int[] myGlobalArray3 = new int[16];
    public static int[] myGlobalArray4 = new int[8];

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    int i=0;
    int k=0;

   // public ArrayList myGlobalArray = null;


            // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                //updateConnectionState(R.string.connected);
//
                Context context1 = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context1, "Connected with a Smart Insole", duration);
                toast.show();

                //invalidateOptionsMenu();
            } else if (BluetoothService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                //updateConnectionState(R.string.disconnected);
               // invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());


            } else if (BluetoothService.ACTION_DATA_AVAILABLE.equals(action)) {
               // if(k==0) {
                    displayData(intent.getStringExtra(BluetoothService.EXTRA_DATA));
//                    Intent myIntent = new Intent(PressureSensorControl2.this, com.example.yashish.pressureinsole.Menu.class);
//                    PressureSensorControl2.this.startActivity(myIntent);
//                    Context context1 = getApplicationContext();
//                    int duration = Toast.LENGTH_SHORT;
//                    Toast toast = Toast.makeText(context1, "Connected with a Smart Insole", duration);
//                    toast.show();
                 //   k++;
                //} else{
                  //  displayData2(intent.getStringExtra(BluetoothService.EXTRA_DATA));
                //}

            }
        }
    };

    private void clearUI() {
        //mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        //mDataField.setText(R.string.no_data);
        moveTaskToBack(true);
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.gatt_services_characteristics);
        //moveTaskToBack(true);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

//



        Intent gattServiceIntent = new Intent(this, BluetoothService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

//

//

//    private void displayData(String data) {
//        if (data != null) {
//
////
//
////            Context context3 = getApplicationContext();
//            String text = data;
////            int duration = Toast.LENGTH_SHORT;
////
//            String[] nums = text.split(" ");
//            myGlobalArray = new int[nums.length];
//            myGlobalArray2= new int[8];
//            int j=0;
//            for(int i = 0; i < myGlobalArray.length; i++)
//            {
//                myGlobalArray[i] = Integer.parseInt(nums[i],16);
//
//                if(i%2 ==1 ){
//                    myGlobalArray2[j]=Integer.parseInt(Integer.toBinaryString(myGlobalArray[i])+Integer.toBinaryString(myGlobalArray[i-1]));
//                    j++;
//                }
//
//            }
////            String num= Integer.toString(myGlobalArray[2]);
////
////            Toast toast = Toast.makeText(context3,num , duration);
////            toast.show();
//
//        }
//    }

    private void displayData(String data) {
        if (data != null) {

//

//            Context context3 = getApplicationContext();
            String text2 = data;
//            int duration = Toast.LENGTH_SHORT;
//
            String[] nums2 = text2.split(" ");
            myGlobalArray3 = new int[nums2.length];
            myGlobalArray4= new int[8];
            int j=0;
            for(int i = 0; i < myGlobalArray3.length; i++)
            {
                myGlobalArray[i] = Integer.parseInt(nums2[i],16);

                if(i%2 ==1 ){
                    myGlobalArray4[j]=Integer.parseInt(Integer.toBinaryString(myGlobalArray3[i])+Integer.toBinaryString(myGlobalArray3[i-1]));
                    j++;
                }

            }
//            String num= Integer.toString(myGlobalArray[2]);
//
//            Toast toast = Toast.makeText(context3,num , duration);
//            toast.show();

        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {

            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                //if (uuid == "0000aa81-0000-1000-8000-00805f9b34fb" ) {

                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
                //}
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }
        if(mGattCharacteristics!=null) {
            BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);

            final int charaProp = characteristic.getProperties();
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                // If there is an active notification on a characteristic, clear
                // it first so it doesn't update the data field on the user interface.
                if (mNotifyCharacteristic != null) {
                    mBluetoothLeService.setCharacteristicNotification(
                            mNotifyCharacteristic, false);
                    mNotifyCharacteristic = null;
                }
                mBluetoothLeService.readCharacteristic(characteristic);
            }
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                mNotifyCharacteristic = characteristic;
                mBluetoothLeService.setCharacteristicNotification(
                        characteristic, true);
            }
        }

//        if(mGattCharacteristics!=null) {
//            BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(1);
//
//            final int charaProp = characteristic.getProperties();
//            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
//                // If there is an active notification on a characteristic, clear
//                // it first so it doesn't update the data field on the user interface.
//                if (mNotifyCharacteristic != null) {
//                    mBluetoothLeService.setCharacteristicNotification(
//                            mNotifyCharacteristic, false);
//                    mNotifyCharacteristic = null;
//                }
//                mBluetoothLeService.readCharacteristic(characteristic);
//            }
//            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
//                mNotifyCharacteristic = characteristic;
//                mBluetoothLeService.setCharacteristicNotification(
//                        characteristic, true);
//            }
//        }
        }
        //mGattServicesList.setAdapter(gattServiceAdapter);




//

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }


}
